import Vue from 'vue/dist/vue'
import App from './App.vue'
import router from './router/client'
import './css/style.css'

Vue.config.productionTip = false

new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App />'
})

if (module.hot) {
  module.hot.accept()
}
