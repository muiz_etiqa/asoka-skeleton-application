import Vue from 'vue/dist/vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const Home = () => import('@/pages/Home.vue')
const Second = () => import('@/pages/Second.vue')
const NotFound = () => import('@/pages/NotFound.vue')

export default new VueRouter({
  mode: 'history',
  routes: [
  {
    path: '/',
    name: 'Index',
    component: Home
  },
  {
    path: '/second',
    name: 'Second',
    component: Second
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound
  }
  ]
})
