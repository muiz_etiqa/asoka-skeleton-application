'use strict'

const merge = require('webpack-merge')
const webpack = require('webpack')
const BaseConfig = require('./webpack.base.config')
const DevConfig = merge(BaseConfig, {
  mode: 'development',
  entry: {
    main: ['webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000', './src/index.js']
  },
  devtool: 'cheap-module-eval-source-map',
})

DevConfig.module.rules.unshift({
  enforce: "pre",
  test: /\.js$/,
  exclude: /node_modules/,
  loader: "eslint-loader",
  options: {
    emitWarning: true,
    failOnError: false,
    failOnWarning: false
  }
})

DevConfig.plugins.push(
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
)

module.exports = DevConfig
